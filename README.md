# torctl

An unfinished and half-assed high-level Rust crate for spawning a Tor process
and interacting with the Tor network.

The goal of this crate is to make it easy to write programs that interact with
the Tor network. This includes using Tor to connect to the rest of the internet
or to create and interact with Onion services. It is *not* a goal to provide a
generic implementation of the Tor control socket API.
