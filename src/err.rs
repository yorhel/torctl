// Short-hand for constructing an Error, saves some typing and import madness
macro_rules! err {
    ($n:tt)          => { crate::err::Error(Box::new(crate::err::ErrorImpl::$n)) };
    ($n:tt, $e:expr) => { crate::err::Error(Box::new(crate::err::ErrorImpl::$n($e))) };
}

#[derive(Debug)]
pub struct Error(pub(crate) Box<ErrorImpl>);

#[derive(Debug)]
pub(crate) enum ErrorImpl {
    IO(std::io::Error), // This could use some context
    ProcShutdown(std::process::ExitStatus),
    ProcTimeout,
    Status(crate::sock::ReplyLine),
    Parse, // This could be more specific
    Keyword(String),
    OnionKey,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use ErrorImpl::*;
        match &*self.0 {
            IO(e)           => write!(f, "IO error: {}", e),
            ProcShutdown(s) => write!(f, "Tor process unexpectedly shutdown with code {}", s),
            ProcTimeout     => write!(f, "Timeout while waiting for Tor process to start"),
            Status(s)       => write!(f, "Unexpected reply ({})", s),
            Parse           => write!(f, "Error parsing reply"),
            Keyword(s)      => write!(f, "Invalid keyword '{}'", s),
            OnionKey        => write!(f, "Invalid Onion private key"),
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Error {
        err!(IO, e)
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        use ErrorImpl::*;
        match &*self.0 {
            IO(e) => Some(e),
            _     => None
        }
    }
}

pub type Result<T> = std::result::Result<T,Error>;
