use std::io::{self,Read,Write};
use std::net::{TcpStream,TcpListener,SocketAddr};
use std::process::Child;
use std::sync::{Mutex,Condvar};

use crate::err::Result;


// Returns an error if the argument is not a "Keyword" in the control protocol. The control spec
// mentions several types of "keyword"s; It is used to specify command names, settings and GETINFO
// keys, and each seem to have different parsing rules. This function is intended to validate user
// input in order to prevent protocol injection. It is therefore lenient enough in that it allows
// for the different "keyword" contexts, but should still be strict enough to prevent the string
// from being interpreted as something other than a keyword.
fn is_keyword_char(c: char) -> bool { c.is_ascii_alphanumeric() || c == '/' || c == '-' || c == '_' || c == '.' }
fn is_keyword(s: &str) -> Result<()> {
    if s.contains(|c| !is_keyword_char(c)) {
        return Err(err!(Keyword, s.to_string()))
    }
    Ok(())
}

// Consume a prefix from a buffer. Handy utility function when parsing.
#[inline]
fn cons(buf: &mut &str, prefix: &str) -> Result<()> {
    if buf.starts_with(prefix) { *buf = &buf[prefix.len()..]; Ok(()) } else { Err(err!(Parse)) }
}

fn cons_keyword<'a>(buf: &mut &'a str) -> Result<&'a str> {
    let i = buf.find(|c| !is_keyword_char(c)).unwrap_or(buf.len());
    if i == 0 {
        Err(err!(Parse))
    } else {
        let s = &buf[..i];
        *buf = &buf[i..];
        Ok(s)
    }
}

fn cons_u8(buf: &mut &str) -> Result<u8> {
    let i = buf.find(|c:char| !c.is_ascii_digit()).unwrap_or(buf.len());
    let v = (&buf[..i]).parse().map_err(|_| err!(Parse))?;
    *buf = &buf[i..];
    Ok(v)
}


// A Reply (Sync or Async) consists of any number of intermediate lines (Mid/Data) followed by an
// End line.
type Reply = Vec<ReplyLine>;


#[derive(Debug,PartialEq)]
pub(crate) struct ReplyLine {
    len:    usize, // Length of this reply, in bytes, including the final CRLF
    status: u16,
    end:    bool,  // If this is a Mid/Data or End reply
    text:   String,
    data:   Vec<u8>
}


// Used in error message formatting
impl<'a> std::fmt::Display for ReplyLine {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} {}", self.status, self.text)
    }
}


impl ReplyLine {
    fn is_async(&self) -> bool {
        self.status >= 600 && self.status < 700
    }

    fn parse_data(buf: &[u8]) -> Option<(usize,Vec<u8>)> {
        // Yes, this actually does need an optimized find_bytes() implementation; With a naive
        // iterator it will take quite a while to parse `GETINFO md/all`.
        let mut off = match twoway::find_bytes(buf, b"\r\n.\r\n") {
            None => return None,
            Some(i) => i
        };
        let mut data = &buf[..off];
        off += 5;

        // Look for a "\r\n." sequence and remove the dot.
        let mut ndata = Vec::new();
        if data.len() > 0 && data[0] == b'.' {
            data = &data[1..];
        }
        while let Some(idx) = twoway::find_bytes(data, b"\r\n.") {
            ndata.extend_from_slice(&data[..idx+2]);
            data = &data[idx+3..];
        }
        ndata.extend_from_slice(data);
        Some((off, ndata))
    }

    // Returns None if there is not enough data in the buffer.
    fn parse(buf: &[u8]) -> Result<Option<ReplyLine>> {
        let mut off = match twoway::find_bytes(buf, b"\r\n") {
            None => return Ok(None),
            Some(i) if i < 4 => return Err(err!(Parse)),
            Some(i) => i
        };
        let line = &buf[..off];
        off += 2;

        let status : u16 = std::str::from_utf8(&line[..3]).map_err(|_| err!(Parse))?.parse().map_err(|_| err!(Parse))?;
        let end = match line[3] {
            b'-' => false,
            b'+' => false,
            b' ' => true,
            _ => return Err(err!(Parse)),
        };
        let text = std::str::from_utf8(&line[4..]).map_err(|_| err!(Parse))?;

        let data = if line[3] == b'+' {
            if let Some((len, d)) = Self::parse_data(&buf[off..]) {
                off += len;
                d
            } else {
                return Ok(None)
            }
        } else {
            Vec::new()
        };

        Ok(Some(ReplyLine {
            len:    off,
            status: status,
            end:    end,
            text:   text.to_string(),
            data:   data,
        }))
    }
}


/// Status event of Tor's bootstrapping process.
#[derive(Debug)]
pub struct BootstrapStatus {
    /// Severity of this event. Typically `Notice` if all is going well, `Warn` if there are some
    /// problems.
    pub severity: Severity,
    /// Bootstrap progress in percent.
    pub progress: u8,
    /// Tor identifier for the current action.
    pub tag:      String, // TODO: Enum?
    /// Human readable description of the next action to take (e.g. "Handshaking with a relay").
    pub summary:  String,
    // TODO: warning/reason/count/recommendation
}

impl BootstrapStatus {
    // Parses the part after the severity and "BOOTSTRAP " string.
    fn parse(sev: Severity, buf: &mut &str) -> Result<Box<BootstrapStatus>> {
        let mut bs = Box::new(BootstrapStatus {
            severity: sev,
            progress: 0,
            tag:      String::new(),
            summary:  String::new(),
        });
        // TODO: Can the order of these arguments change?
        cons(buf, "PROGRESS=")?;
        bs.progress = cons_u8(buf)?;
        cons(buf, " TAG=")?;
        bs.tag = cons_keyword(buf)?.to_owned();
        cons(buf, " SUMMARY=")?;
        bs.summary = QuotedString::parse(buf)?;
        Ok(bs)
    }
}


/// Tor log message severity.
#[derive(Debug,PartialEq,Eq,PartialOrd,Ord,Clone,Copy)]
pub enum Severity {
    Debug,
    Info,
    Notice,
    Warn,
    Err
}

impl Severity {
    fn parse(buf: &mut &str) -> Result<Severity> {
             if cons(buf, "DEBUG" ).is_ok() { Ok(Severity::Debug ) }
        else if cons(buf, "INFO"  ).is_ok() { Ok(Severity::Info  ) }
        else if cons(buf, "NOTICE").is_ok() { Ok(Severity::Notice) }
        else if cons(buf, "WARN"  ).is_ok() { Ok(Severity::Warn  ) }
        else if cons(buf, "ERR"   ).is_ok() { Ok(Severity::Err   ) }
        else { Err(err!(Parse)) }
    }
}


/// An asynchronous event received from Tor. These are returned by `read_event()`.
#[derive(Debug)]
pub enum Event {
    /// Log messages
    Log(Severity, String),
    /// Bootstrap status event
    Bootstrap(Box<BootstrapStatus>),
}


/// A Builder type for the argument to `setevents()`. Use `EventList::default()` to initialize a
/// default (empty) event list.
///
/// ```
/// # use torctl::{EventList,Severity};
/// let log_warn_and_error = EventList::default().log(Severity::Notice);
/// ```
#[derive(Debug,Default,Clone,Copy)]
pub struct EventList {
    log:       Option<Severity>,
    bootstrap: bool,
}

impl EventList {
    /// Receive log messages matching the given severity or higher.
    pub fn log(mut self, lvl: Severity) -> Self { self.log = Some(lvl); self }

    /// Receive `Bootstrap` events.
    pub fn bootstrap(mut self) -> Self { self.bootstrap = true; self }

    fn cmd(&self) -> String {
        let mut s = "SETEVENTS".to_string();
        match self.log {
            Some(Severity::Debug)  => s.push_str(" ERR WARN NOTICE INFO DEBUG"),
            Some(Severity::Info)   => s.push_str(" ERR WARN NOTICE INFO"),
            Some(Severity::Notice) => s.push_str(" ERR WARN NOTICE"),
            Some(Severity::Warn)   => s.push_str(" ERR WARN"),
            Some(Severity::Err)    => s.push_str(" ERR"),
            None => (),
        }
        if self.bootstrap { s.push_str(" STATUS_CLIENT") }
        s.push_str("\r\n");
        s
    }
}



// A double-quoted string where only \ and " are escaped.
// Encoding: QuotedString("hello").to_string()
// Decoding: QuotedString::parse("\"hello\"")
struct QuotedString<'a>(&'a str);

impl<'a> std::fmt::Display for QuotedString<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use std::fmt::Write;
        f.write_char('"')?;
        for c in self.0.chars() {
            if c == '"' || c == '\\' {
                f.write_char('\\')?;
            }
            f.write_char(c)?;
        }
        f.write_char('"')
    }
}

impl<'a> QuotedString<'a> {
    fn parse(s: &mut &str) -> Result<String> {
        cons(s, "\"")?;
        let mut out = String::new();
        let mut q = false;
        for (i, c) in s.char_indices() {
            match (q, c) {
                (true , _   ) => { q = false; out.push(c) },
                (false, '\\') => q = true,
                (false, '"' ) => { *s = &s[i+1..]; return Ok(out) },
                (false, _   ) => out.push(c),
            }
        }
        Err(err!(Parse))
    }
}




/// Tor process and control socket. Use `spawn()` to create an instance of this struct.
///
/// A Tor struct can be used from multiple threads, with the following limitations:
/// - It is possible to call methods simultaneously from multiple threads, but the order in which
///   they are executed is, of course, not deterministic.
/// - You should only call `read_event()` from a single thread at a time. The API allows for
///   multiple threads to read events, but an event will only be received by one thread.
pub struct Tor {
    sock:    TcpStream,
    child:   Child,
    socks:   SocketAddr,
    // Read buffer - only one thread can be reading from the socket at a time.
    rdbuf:   Mutex<Vec<u8>>,
    // Read queue - if multiple threads are interested in reading from the socket, then this
    // structure is used to coordinate which thread gets to read from the socket. If that thread
    // happens to read a reply that it isn't interested in (e.g. it expected a sync reply and got
    // an async one), then that reply is pushed to the queue.
    queue:   Mutex<TorQueue>,
    queuecv: Condvar,
    // Write mutex - this ensures only a single command can be sent at a time. This mutex is
    // currently also held while reading the response of a command, because the read queue itself
    // does not ensure that sync replies are consumed by different threads in the proper order.
    // (Could probably be fixed by keeping track of a command sequence - Tor does support
    // pipelining multiple commands, so this is a potential performance improvement)
    writer:  Mutex<bool>,
}

struct TorQueue {
    queue:   Vec<Reply>,
    reading: bool,
}


impl Drop for Tor {
    fn drop(&mut self) {
        // We have to wait() for the child to shut down, otherwise we get a zombie process. This is
        // a quick-and-dirty approach which doesn't really give Tor the time to do a clean
        // shutdown.  In fact, we kill the process before even shutting down the socket.
        // A cleaner "shut down socket and wait a bit" approach should probably be implemented as a
        // separate method.
        self.child.kill().is_ok();
        self.child.wait().is_ok();
    }
}


impl Tor {
    fn read_line(buf: &mut Vec<u8>, sock: &TcpStream) -> Result<ReplyLine> {
        // This buffer handling isn't very efficient, but that's probably fine.
        loop {
            if let Some(r) = ReplyLine::parse(&buf[..])? {
                buf.drain(..r.len);
                return Ok(r);
            }

            let mut rd = [0u8; 512];
            match (&*sock).read(&mut rd) {
                Err(ref e) if e.kind() == io::ErrorKind::Interrupted => (),
                Err(e) => return Err(e.into()),
                Ok(0) => return Err(io::Error::new(io::ErrorKind::UnexpectedEof, "Unexpected disconnect").into()),
                Ok(l) => buf.extend_from_slice(&rd[..l])
            }
        }
    }

    fn read_reply(buf: &mut Vec<u8>, sock: &TcpStream) -> Result<Reply> {
        let mut reply : Reply = Vec::new();
        while reply.last().map(|r| !r.end).unwrap_or(true) {
            reply.push(Self::read_line(buf, sock)?);
        }
        Ok(reply)
    }

    fn get_reply(&self, async_event: bool) -> Result<Reply> {
        // Check the queue to see if our reply has already been read.
        {
            let mut queue = self.queue.lock().unwrap();
            loop {
                if let Some(idx) = queue.queue.iter().position(|r| r[0].is_async() == async_event) {
                    return Ok(queue.queue.remove(idx))
                }
                // Nothing in the queue, but another thread is currently reading, so let's wait.
                if queue.reading {
                    queue = self.queuecv.wait(queue).unwrap();
                // Nothing in the queue and nobody else is reading, let's read from the socket.
                } else {
                    queue.reading = true;
                    break;
                }
            }
        }

        // We now have the read flag, so keep reading until we have our reply.
        let ret = loop {
            let mut buf = self.rdbuf.lock().unwrap();
            match Self::read_reply(&mut buf, &self.sock) {
                Err(e) => break Err(e),
                Ok(r) => {
                    if r[0].is_async() == async_event {
                        break Ok(r)
                    // This reply isn't for us, add it to the queue
                    } else {
                        self.queue.lock().unwrap().queue.push(r);
                        self.queuecv.notify_one();
                    }
                }
            }
        };
        self.queue.lock().unwrap().reading = false;
        self.queuecv.notify_one();
        ret
    }

    fn cmd<S: AsRef<[u8]>>(&self, cmd: S) -> Result<Reply> {
        let _guard = self.writer.lock().unwrap();

        (&self.sock).write_all(cmd.as_ref())?;
        let mut ret = self.get_reply(false)?;
        if ret[0].status >= 200 && ret[0].status < 300 {
            Ok(ret)
        } else {
            Err(err!(Status, ret.remove(0)))
        }
    }

    pub(crate) fn connect_child(s: &SocketAddr, pass: &str, child: Child) -> Result<Tor> {
        let mut tor = Tor {
            sock:    TcpStream::connect(s)?,
            child:   child,
            socks:   "0.0.0.0:0".parse().unwrap(),
            rdbuf:   Mutex::new(Vec::new()),
            queue:   Mutex::new(TorQueue {
                queue:   Vec::new(),
                reading: false,
            }),
            queuecv: Condvar::new(),
            writer:  Mutex::new(true),
        };
        tor.cmd(format!("AUTHENTICATE {}\r\n", QuotedString(pass)))?;
        tor.cmd("TAKEOWNERSHIP\r\n")?;
        tor.resetconf(&[("__OwningControllerProcess", None)])?;

        let ret = tor.getinfo(&["net/listeners/socks"])?;
        let mut buf = &ret[0].1[..];
        tor.socks = QuotedString::parse(&mut buf)?.parse().map_err(|_| err!(Parse))?;

        Ok(tor)
    }

    // XXX: This IntoIterator works with &[..] and &vec![..]. Haven't tested HashMap/BTreeMap yet,
    // but I suspect their signature doesn't match.
    fn setresetconf<'a,T>(&self, mut msg: String, settings: T) -> Result<()>
        where T: IntoIterator<Item = &'a (&'a str, Option<&'a str>)>,
    {
        use std::fmt::Write;
        for (k, v) in settings {
            is_keyword(k)?;
            msg.push(' ');
            msg.push_str(k);
            if let Some(v) = v {
                write!(msg, "={}", QuotedString(v)).is_ok();
            }
        }
        msg.push_str("\r\n");
        self.cmd(msg).map(|_|())
    }

    /* Send a SETCONF command.
    fn setconf<'a,T>(&self, settings: T) -> Result<()>
        where T: IntoIterator<Item = &'a (&'a str, Option<&'a str>)>
    {
        self.setresetconf("SETCONF".to_string(), settings)
    }*/

    // Send a RESETCONF command.
    fn resetconf<'a,T>(&self, settings: T) -> Result<()>
        where T: IntoIterator<Item = &'a (&'a str, Option<&'a str>)>
    {
        self.setresetconf("RESETCONF".to_string(), settings)
    }

    // Get run-time Tor variables. An error is returned if any of the requested keys does not
    // exist.
    //
    // Corresponds to the GETINFO command. Refer to the GETINFO documentation in the [tor-control
    // specification](https://gitweb.torproject.org/torspec.git/blob/control-spec.txt) for the
    // list of accepted keys.
    fn getinfo<'a,T: IntoIterator<Item = &'a &'a str>>(&self, keys: T) -> Result<Vec<(String, String)>> {
        let mut msg = "GETINFO".to_string();
        for k in keys {
            is_keyword(k)?;
            msg.push(' ');
            msg.push_str(k);
        }
        msg.push_str("\r\n");

        let mut res = Vec::new();
        for line in self.cmd(msg)? {
            if line.text == "OK" {
                break;
            }
            if let Some(is) = line.text.find('=') {
                let val = if line.data.len() > 0 {
                    String::from_utf8(line.data).map_err(|_|err!(Parse))?
                } else {
                    (&line.text[is+1..]).to_string()
                };
                res.push(( (&line.text[..is]).to_string(), val ));
            } else {
                return Err(err!(Parse));
            }
        }
        Ok(res)
    }

    /// Returns the address of the Tor SOCKS proxy.
    pub fn socks_addr(&self) -> SocketAddr {
        self.socks
    }

    /// Open a Onion service at the specified (virtual) port. Returns its address (xyz.onion),
    /// private key and listen socket. If no key is given, a new one is generated automatically.
    /// The given key should be prefixed by its type (e.g.: `ED25519-V3:xxx`). the returned key
    /// will have this prefix.
    // TODO: Using a String for the key is ugly and easily confused with the public address, might
    // want to use a separate type? (Or, perhaps even better, a return type encompassing all three
    // values and having a Drop implementation that calls DEL_ONION).
    pub fn listen_onion(&self, virtual_port: u16, key: Option<&str>) -> Result<(String, String, TcpListener)> {
        let key = key.unwrap_or("NEW:ED25519-V3"); // tor 0.3.5.8 still defaults to v2 with "NEW:BEST" for some reason, just enforce v3 for now.
        if key.contains(|c: char| c.is_whitespace()) { // Just to prevent protocol injection.
            return Err(err!(OnionKey));
        }
        let listen = TcpListener::bind("127.0.0.1:0")?;
        let local_port = listen.local_addr()?.port();
        let r = self.cmd(format!("ADD_ONION {} Port={},127.0.0.1:{}\r\n", key, virtual_port, local_port))?;

        let mut service = None;
        let mut rkey = key.to_string();
        for line in r {
            if line.status != 250 {
                return Err(err!(Parse));
            }
            if line.text.starts_with("ServiceID=") {
                service = Some(format!("{}.onion", &line.text[10..]));
            } else if line.text.starts_with("PrivateKey=") {
                rkey = (&line.text[11..]).to_string();
            }
        }
        match service {
            None    => return Err(err!(Parse)),
            Some(s) => return Ok((s, rkey, listen))
        }
    }

    /// Returns the most recent bootstrap status.
    pub fn bootstrap_status(&self) -> Result<Box<BootstrapStatus>> {
        let ret = self.getinfo(&["status/bootstrap-phase"])?;
        let mut buf = &ret[0].1[..];
        let sev = Severity::parse(&mut buf)?;
        cons(&mut buf, " BOOTSTRAP ")?;
        BootstrapStatus::parse(sev, &mut buf)
    }

    /// Configure which events to receive with `read_events()`.
    pub fn set_events(&self, events: EventList) -> Result<()> {
        self.cmd(events.cmd()).map(|_|())
    }

    /// Read the next event from the socket. This method blocks until an event has been received.
    // TODO: Differentiate between fatal and temporary errors.
    // TODO: Some way to wake up a blocked read_event (e.g. to shut down the socket).
    pub fn read_event(&self) -> Result<Event> {
        loop {
            let mut ret = self.get_reply(true)?;
            let ev = ret.remove(0);
            let mut buf = &ev.text[..];

            if let Ok(sev) = Severity::parse(&mut buf) {
                let s = if ev.data.is_empty() {
                    buf.trim().to_owned()
                } else {
                    String::from_utf8_lossy(&ev.data).trim().to_owned()
                };
                return Ok(Event::Log(sev, s))

            } else if cons(&mut buf, "STATUS_CLIENT ").is_ok() {
                let sev = Severity::parse(&mut buf)?;
                if cons(&mut buf, " BOOTSTRAP ").is_ok() {
                    return Ok(Event::Bootstrap(BootstrapStatus::parse(sev, &mut buf)?))
                }
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_replyline() {
        assert!(ReplyLine::parse(b"\r\n").is_err());
        assert!(ReplyLine::parse(b"250\r\n").is_err());
        assert!(ReplyLine::parse(b"2500 Text\r\n").is_err());
        assert!(ReplyLine::parse(b"250.Text\r\n").is_err());
        assert!(ReplyLine::parse(b".50 Text\r\n").is_err());
        assert!(ReplyLine::parse(b"-50 Text\r\n").is_err());
        assert!(ReplyLine::parse(b"01x Text\r\n").is_err());

        assert_eq!(ReplyLine::parse(b"").unwrap(), None);
        assert_eq!(ReplyLine::parse(b"250 Hello\r").unwrap(), None);
        assert_eq!(ReplyLine::parse(b"250+Hello\r\n").unwrap(), None);
        assert_eq!(ReplyLine::parse(b"250+Hello\r\ndata\r\n..a\r\n.\r").unwrap(), None);

        assert_eq!(ReplyLine::parse(b"250 \r\n").unwrap(), Some(ReplyLine { len: 6, status: 250, end: true, text: "".to_string(), data: Vec::new() }) );
        assert_eq!(ReplyLine::parse(b"650-Hello\r\n").unwrap(), Some(ReplyLine { len: 11, status: 650, end: false, text: "Hello".to_string(), data: Vec::new() }) );
        assert_eq!(ReplyLine::parse(b"650+Hello\r\n.data\r\n.\r\n").unwrap(), Some(ReplyLine { len: 21, status: 650, end: false, text: "Hello".to_string(), data: b"data".to_vec() }) );
        assert_eq!(ReplyLine::parse(b"650+Hello\r\ndata\r\n..\r\n.\r\n").unwrap(), Some(ReplyLine { len: 24, status: 650, end: false, text: "Hello".to_string(), data: b"data\r\n.".to_vec() }) );
    }

    #[test]
    fn qs_fmt() {
        assert_eq!(QuotedString("").to_string(), "\"\"".to_string());
        assert_eq!(QuotedString(" \\a\"b \x02").to_string(), "\" \\\\a\\\"b \x02\"".to_string());
    }

    #[test]
    fn qs_parse() {
        let mut s = "";
        assert!(QuotedString::parse(&mut s).is_err());
        let mut s = "\"";
        assert!(QuotedString::parse(&mut s).is_err());
        let mut s = "\" \\\"";
        assert!(QuotedString::parse(&mut s).is_err());
        let mut s = "abc";
        assert!(QuotedString::parse(&mut s).is_err());

        let mut s = "\"\"";
        assert_eq!(QuotedString::parse(&mut s).unwrap(), "".to_string());
        assert_eq!(s, "");

        let mut s = "\" \\\\a\\\"b \x02\"";
        assert_eq!(QuotedString::parse(&mut s).unwrap(), " \\a\"b \x02".to_string());
        assert_eq!(s, "");

        let mut s = "\"\\r\\n\" hey";
        assert_eq!(QuotedString::parse(&mut s).unwrap(), "rn".to_string());
        assert_eq!(s, " hey");
    }
}
