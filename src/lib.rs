#[macro_use] mod err;
mod process;
mod sock;

pub use err::Error;
pub use sock::{Tor,Event,EventList,Severity,BootstrapStatus};
pub use process::{genpass,hashpass,spawn};
