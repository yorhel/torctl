use std::sync::Arc;

fn main() {
    let s = Arc::new( torctl::spawn("/usr/bin/tor", "tor-data", "").unwrap() );

    let slisten = s.clone();
    std::thread::spawn(move || {
        loop {
            match slisten.read_event() {
                Ok(ev) => println!("Event: {:?}", ev),
                Err(e) => {
                    println!("Error reading event: {}", e);
                    break;
                },
            }
        }
    });
    s.set_events( torctl::EventList::default().log(torctl::Severity::Notice).bootstrap() ).unwrap();

    dbg!(s.socks_addr());
    //s.setconf(&[("SocksPort", Some("10245"))]).unwrap();
    //dbg!(s.getconf(&["ContactInfo", "DataDirectory", "HiddenServiceOptions"]).unwrap());
    //dbg!(s.getinfo(&["version", "config-text", "net/listeners/socks"]).unwrap());

    std::thread::sleep(std::time::Duration::from_secs(5));
    //s.quit().unwrap();

    std::thread::sleep(std::time::Duration::from_secs(500));
}
